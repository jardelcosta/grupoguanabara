﻿using Guanabara.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Guanabara.Services
{
    public class PostsService : IPostService
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly string apiUrl = "https://jsonplaceholder.typicode.com/posts/";

        public PostsService(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<HttpResponseMessage> Get()
        {
            return await CreateRequest(apiUrl, HttpMethod.Get);
        }

        public async Task<HttpResponseMessage> Get(int id)
        {
            return await CreateRequest(apiUrl + id, HttpMethod.Get);
        }

        public async Task<HttpResponseMessage> Post(PostModel model)
        {
            return await CreateRequest(apiUrl, HttpMethod.Post, JsonConvert.SerializeObject(model));
        }

        public async Task<HttpResponseMessage> Put(PostModel model)
        {
            return await CreateRequest(apiUrl + model.Id, HttpMethod.Put, JsonConvert.SerializeObject(model));
        }

        public async Task<HttpResponseMessage> Delete(int id)
        {
            return await CreateRequest(apiUrl, HttpMethod.Delete);
        }


        public async Task<HttpResponseMessage> CreateRequest(string endpoint, HttpMethod method, string content = null)
        {
            HttpRequestMessage request = new HttpRequestMessage(method, new Uri(endpoint));
            request.Headers.Add("Accept", "application/json");

            if (content != null)
                request.Content = new StringContent(content, Encoding.UTF8, "application/json");

            var client = _clientFactory.CreateClient();
            return await client.SendAsync(request);
        }
    }
}
