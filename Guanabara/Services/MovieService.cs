﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Guanabara.Services
{
    public class MovieService : IMovieService
    {
        private readonly IHttpClientFactory _clientFactory;

        public MovieService(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<HttpResponseMessage> GetMovie(int id)
        {
            return await CreateRequest(string.Format("https://jsonplaceholder.typicode.com/users/{0}", id), HttpMethod.Get);
        }

        public async Task<HttpResponseMessage> CreateRequest(string endpoint, HttpMethod method)
        {
            HttpRequestMessage request = new HttpRequestMessage(method, new Uri(endpoint));
            request.Headers.Add("Accept", "application/json");

            var client = _clientFactory.CreateClient();
            return await client.SendAsync(request);
        }
    }
}
