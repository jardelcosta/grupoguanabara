﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Guanabara.Services
{
    public interface IMovieService
    {
        Task<HttpResponseMessage> CreateRequest(string endpoint, HttpMethod method);
        Task<HttpResponseMessage> GetMovie(int id);
    }
}
