﻿using Guanabara.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Guanabara.Services
{
    public interface IPostService
    {
        Task<HttpResponseMessage> Get();
        Task<HttpResponseMessage> Get(int id);
        Task<HttpResponseMessage> Post(PostModel post);
        Task<HttpResponseMessage> Put(PostModel post);
        Task<HttpResponseMessage> Delete(int id);
    }
}