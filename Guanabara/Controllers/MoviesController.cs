﻿using Guanabara.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Guanabara.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MoviesController : ControllerBase
    {
        private IMovieService _service;

        public MoviesController(IMovieService service)
        {
            _service = service;
        }


        [HttpGet("movie/{id}")]
        public async Task<IActionResult> Movie(int id)
        {
            var response = await _service.GetMovie(id);
            var responseBody = await response.Content.ReadAsStringAsync();
           
            if ((int)response.StatusCode >= 200 && (int)response.StatusCode <= 299)
                return Ok(responseBody);

            return BadRequest(responseBody);
        }
    }
}