﻿using Guanabara.Models;
using Guanabara.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Guanabara.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PostsController : ControllerBase
    {
        private IPostService _postService;

        public PostsController(IPostService service)
        {
            _postService = service;
        }


        public async Task<IActionResult> Get()
        {
            var response = await _postService.Get();
            var responseBody = await response.Content.ReadAsStringAsync();
           
            if ((int)response.StatusCode >= 200 && (int)response.StatusCode <= 299)
                return Ok(responseBody);

            return BadRequest(responseBody);
        }


        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var response = await _postService.Get(id);
            var responseBody = await response.Content.ReadAsStringAsync();

            if ((int)response.StatusCode >= 200 && (int)response.StatusCode <= 299)
                return Ok(responseBody);

            return BadRequest(responseBody);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] PostModel model)
        {
            var response = await _postService.Post(model);

            if ((int)response.StatusCode >= 200 && (int)response.StatusCode <= 299)
                return Ok(model.Id);

            return BadRequest(model);
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] PostModel model)
        {
            var response = await _postService.Put(model);

            if ((int)response.StatusCode >= 200 && (int)response.StatusCode <= 299)
                return Ok(model.Id);

            return BadRequest(model);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            var response = await _postService.Delete(id);

            if ((int)response.StatusCode >= 200 && (int)response.StatusCode <= 299)
                return Ok();

            return BadRequest();
        }

    }
}